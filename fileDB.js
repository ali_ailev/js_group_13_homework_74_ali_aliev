const fs = require('fs');

const path = './messages';

let messages = [];
let lastFive = [];

module.exports = {
    init() {
        try {
            fs.readdir(path, (err, files) => {
                files.forEach(file => {
                    const fileContents = fs.readFileSync(path + '/' + file);
                    messages.push(JSON.parse(fileContents));
                });
            });
        } catch (e) {
            messages = [];
        }
    },
    getMessages() {
        for (let i = messages.length; i >= messages.length - 5; i--) {
            if (messages[i] !== null && messages[i] !== undefined) {
                lastFive.push(messages[i]);
            }
        }
        return lastFive;
    },

    addMessage(filename, message) {
        messages.push(message);
        fs.writeFileSync("./messages/" + filename + ".json", JSON.stringify(message));
    },
};