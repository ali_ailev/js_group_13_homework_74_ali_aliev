const express = require('express');
const messages = require('./app/messages');
const file = require('./fileDB');
const app = express();

const port = 8000;

app.use(express.json());
app.use('/messages', messages);

file.init();


app.listen(port, () => {
    console.log(`Servers port: ${port}`);
})