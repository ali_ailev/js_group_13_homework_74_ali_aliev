const express = require('express');
const router = express.Router();
const file = require('../fileDB');
const date = new Date();

router.get('/', (req, res) => {
   const messages = file.getMessages();
   return res.send(messages);
});

router.post('/', (req, res) => {
    const message = {
        message : req.body.message,
        dataTime : date,
    }
    file.addMessage(message.dataTime,message);
    return res.send(message);
});




module.exports = router;